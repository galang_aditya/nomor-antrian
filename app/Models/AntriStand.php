<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AntriStand extends Model
{
    use HasFactory;

    protected $fillabel =
    [   'nama',
        'email',
        'tanggal_pesan',
        'kd_stan',
        'nomor_antrian'
    ];
}
