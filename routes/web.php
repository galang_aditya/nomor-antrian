<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AntrianController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect()->route('antrian');
});

Route::get('antrian',[AntrianController::class,'index'])->name('antrian');
Route::post('kode-atrian',[AntrianController::class,'kodeAntrian'])->name('kode');
