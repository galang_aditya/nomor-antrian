<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\KodeAtrian;

class KodeAntrianSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        KodeAtrian::create([
            'nama_stand' => 'Foto',
            'kode_stand' => 'FT'
        ]);
        KodeAtrian::create([
            'nama_stand' => 'Lukis',
            'kode_stand' => 'LK'
        ]);
    }
}
