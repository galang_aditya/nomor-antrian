<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\KodeAtrian;
use App\Models\AntriStand;
use App\Models\NomorUrutft;
use App\Models\NomorUrutlk;
use App\Http\Requests\BuatAntrianRequest;
use SebastianBergmann\LinesOfCode\Counter;
use Carbon\Carbon;
use PhpParser\Node\Stmt\ElseIf_;

class AntrianController extends Controller
{
    //
    public function index(){
        $code = KodeAtrian::all();
        return view('Antrian.index',compact('code'));
    }
    public function kodeAntrian(BuatAntrianRequest $request){

        //nomor urut
        $counter_fk = NomorUrutft::max('id')+1;
        $counter_lk = NomorUrutlk::max('id')+1;
        //kode pendaftaran
        $kodeft = $request->kd_stand.date('Ymd').str_pad($counter_fk, 3, '0', STR_PAD_LEFT);
        $kodelk = $request->kd_stand.date('Ymd').str_pad($counter_lk, 3, '0', STR_PAD_LEFT);

        //cek jumlah pendaftar
        $antrain_ft = AntriStand::where(function($query){
            $query->where('kd_stand',['FT']);
        })->where('tanggal_pesan',$request->tanggal_pesan)->count();
        $antrain_lk = AntriStand::where(function($query){
            $query->where('kd_stand',['LK']);
        })->where('tanggal_pesan',$request->tanggal_pesan)->count();


        //simpan data pemesanan jika kuto masih ada
        if ($antrain_ft >= 50 && $request->kd_stand == "FT"){
            return redirect()->route('antrian')->with('message','data pemesanan hari ini sudah penuh');
        }elseif ($request->kd_stand == "FT" && $antrain_ft <= 50){
            //simpan nomor urut
            $nomor_ft = new NomorUrutft;
            $nomor_ft->nomo_ft = $counter_fk;
            $nomor_ft->created_at = $request->tanggal_pesan;
            $nomor_ft->save();
            //simpan data kategori Foto
            $data = new AntriStand;
            $data->nama = $request->nama;
            $data->email = $request->email;
            $data->tanggal_pesan = $request->tanggal_pesan;
            $data->kd_stand = $request->kd_stand;
            $data->nomor_antrian = $kodeft;
            $data->save();
            $members = AntriStand::where('nomor_antrian',$kodeft)->limit(1)->get();
            return view('Antrian.nomor',compact('members'));
        }elseif($request->kd_stand == "LK" && $antrain_lk <= 50){
            //simpan nomor urut
            $nomor_ft = new NomorUrutlk;
            $nomor_ft->nomo_lk = $antrain_lk;
            $nomor_ft->created_at = $request->tanggal_pesan;
            $nomor_ft->save();
            //simpan data kategori Lukis
            $data = new AntriStand;
            $data->nama = $request->nama;
            $data->email = $request->email;
            $data->tanggal_pesan = $request->tanggal_pesan;
            $data->kd_stand = $request->kd_stand;
            $data->nomor_antrian = $antrain_lk;
            $data->save();
            $members = AntriStand::where('nomor_antrian',$antrain_lk)->limit(1)->get();
            return view('Antrian.nomor',compact('members'));
        }elseif($antrain_ft >= 50 && $request->kd_stand == "LK"){
            return redirect()->route('antrian')->with('message','data pemesanan hari ini sudah penuh');
        }
    }public function cektakTiket(Request $request){

    }
}
