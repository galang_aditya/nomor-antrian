<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KodeAtrian extends Model
{
    use HasFactory;

    protected $fillabel =
    [
        'nama_stand',
        'kode_stand',
    ];
}
