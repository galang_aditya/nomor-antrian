<!DOCTYPE html>
<html>
<style>
input[type=text], select {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}

input[type=date], select {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}

input[type=submit] {
  width: 100%;
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}

input[type=submit]:hover {
  background-color: #45a049;
}

div {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 200px;
  width: 500px;
  margin: auto;
}
</style>
<body>
<div>
  <form action="{{ route('kode') }}" method="POST">
    @csrf

    <label for="country">Pilih Stand</label>
    <select id="country" name="kd_stand" required>
        <option selected disabled>Buka Untuk Memilih</option>
        @foreach ( $code as $kode )
        <option value="{{ $kode->kode_stand }}">{{ $kode->nama_stand }}</option>
        @endforeach
    </select>

    <label for="fname">Nama</label>
    <input type="text" id="fname" name="nama" placeholder="nama" required>

    <label for="lname">Email</label>
    <input type="text" id="lname" name="email" placeholder="email" required>

    <label for="lname">Tanggal Pemesanan</label>
    <input type="date" id="lname" name="tanggal_pesan" required>

    <input type="submit" value="Buat Pesanan">
  </form>
</div>

</body>
</html>


